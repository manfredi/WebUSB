# Examining the WebUSBs
## Overview
This project examined the security and privacy implications of two new web technologies available in the Chrome browser: WebUSB and Web Bluetooth. WebUSB gives websites access to USB devices connected to the host's computer, and Web Bluetooth gives websites access to Bluetooth Low Energy devices near the host's computer.

For more information, see our [slides](WebUSB_WebBluetooth_Slides.pdf) and [paper](WebUSB_WebBluetooth_Paper.pdf).