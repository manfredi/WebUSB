## Malicious iframe test
This is to see if malicious iframes from other websites can access the device. They can't, because the Feature Policy API does not let them access navigator.usb, and the "WebUSB not available" message will be shown. If you view the source, you'll see the iframe contains a link to the WebUSB page that, on its own, works fine.

To run this and confirm that it doesn't work, from the directory with index.html, type:

`python -m SimpleHTTPServer`

And navigate to the IP and port number where Python says it's hosting it.
