## To run:
1. Install pyopenssl: `sudo pip install pyopenssl`
2. From the directory where index.html is, run `sudo python SimpleSecureHTTPServer.py`
3. Go to https://127.0.0.1:443 in your browser
