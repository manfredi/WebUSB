<?php

require_once("include/html_functions.php");
require_once("include/guestbook.php");

if (isset($_POST["name"]) && isset($_POST["comment"]))
{
   if ($_POST['name'] == "" || $_POST['comment'] == "")
   {
      $flash['error'] = "Must include both the name and comment field!";
   }
   else
   {
      $res = Guestbook::add_guestbook($_POST["name"], $_POST["comment"], False);
      if (!$res)
      {
     die(mysql_error());
      }      
   }
}

$guestbook = Guestbook::get_all_guestbooks();
?>

<?php our_header("guestbook"); ?>

<div class="column prepend-1 span-24 first last">
<!-- Origin Trial Token, feature = WebUSB (For Chrome M57+), origin = http://localhost:8000, expires = 2017-07-12 -->
<meta http-equiv="origin-trial" data-feature="WebUSB (For Chrome M57+)" data-expires="2017-07-12" content="AkMym4kXwhtzEgAH7LrwEeD+f6tCGP321WiROL/488ExWDQLmdKAmfYGK0ytMAVmqBWn48t5iQf3I5dkQGIk4gcAAABKeyJvcmlnaW4iOiJodHRwOi8vbG9jYWxob3N0OjgwMDAiLCJmZWF0dXJlIjoiV2ViVVNCMiIsImV4cGlyeSI6MTQ5OTgzODIwN30=">
<!-- Origin Trial Token, feature = WebUSB (For Chrome M57+), origin = https://outerspace.today, expires = 2017-07-12 -->
<meta http-equiv="origin-trial" data-feature="WebUSB (For Chrome M57+)" data-expires="2017-07-12" content="AkI5QJVB9TuoSh9lOKrlHSAZu1FDzPkg5BpHd5zCvCTsI+h1yRTuBVkOiFNhwRsNZonCglPC4HSX7a6ABMUM1AcAAABReyJvcmlnaW4iOiJodHRwczovL291dGVyc3BhY2UudG9kYXk6NDQzIiwiZmVhdHVyZSI6IldlYlVTQjIiLCJleHBpcnkiOjE0OTk4MzgyMDd9">  
 <!-- Origin Trial Token, feature = WebUSB (For Chrome M57+), origin = https://outerspace.today, expires = 2017-05-01 -->
    <meta http-equiv="origin-trial" data-feature="WebUSB (For Chrome M57+)" data-expires="2017-05-01" content="As1zP2mDJIxrdQVBxFHoz97RnfTvH7SA1Gxdq2pfFip71eOnh1TQllHQBJB5zt2l9FiDComDXapx97/CzTuwwgYAAABReyJvcmlnaW4iOiJodHRwczovL291dGVyc3BhY2UudG9kYXk6NDQzIiwiZmVhdHVyZSI6IldlYlVTQjIiLCJleHBpcnkiOjE0OTM2NDIxODF9"> 
    <!-- Origin Trial Token, feature = WebUSB (For Chrome M57+), origin = http://localhost:8000, expires = 2017-05-01 -->
    <meta http-equiv="origin-trial" data-feature="WebUSB (For Chrome M57+)" data-expires="2017-05-01" content="Ag1BWt17hDgrQZrwC4VlUBDhBHRDxKUrIyRRCqM2J2WkfHm6Q5L76nKzMzcW/qKmtk2BuzdjF/l94hVNT7r1bAUAAABKeyJvcmlnaW4iOiJodHRwOi8vbG9jYWxob3N0OjgwMDAiLCJmZWF0dXJlIjoiV2ViVVNCMiIsImV4cGlyeSI6MTQ5MzY0MjE4MX0="> 
    <!-- Origin Trial Token, feature = WebUSB (For Chrome M57+), origin = https://outerspace.today, expires = 2017-06-06 -->
    <meta http-equiv="origin-trial" data-feature="WebUSB (For Chrome M57+)" data-expires="2017-06-06" content="At6tB/w4EqgfH/6mwSrMMvKIgFycexveM7Q1zSHcNt/ubMEMFWVKHZxAKtTPI7CriDmXK+n8KgHf0cnA3xjlvwAAAABReyJvcmlnaW4iOiJodHRwczovL291dGVyc3BhY2UudG9kYXk6NDQzIiwiZmVhdHVyZSI6IldlYlVTQjIiLCJleHBpcnkiOjE0OTY4MDg5NDJ9"> 
    <!-- Origin Trial Token, feature = WebUSB (For Chrome M57+), origin = http://localhost:8000, expires = 2017-06-06 -->
    <meta http-equiv="origin-trial" data-feature="WebUSB (For Chrome M57+)" data-expires="2017-06-06" content="AqnsnZ4Mv1oEUY4I/kEa90X0VypklKeeqtBwzJNuITd6sQWl6GsTOemxDzdG2Q6yKLwhOBwbAdpFWjfxJRA/ewQAAABKeyJvcmlnaW4iOiJodHRwOi8vbG9jYWxob3N0OjgwMDAiLCJmZWF0dXJlIjoiV2ViVVNCMiIsImV4cGlyeSI6MTQ5NjgwODk0Mn0=">

<h2>Guestbook</h2>
<?php error_message(); ?>
<h4>See what people are saying about us!</h4>

<?php
   if ($guestbook)
   { 
     foreach ($guestbook as $guest)
     {
    ?>
    <p class="comment"><?= $guest["comment"] ?></p>
    <p> - by <?=h( $guest["name"] ) ?> </p>
    <?php
     } ?>
<?php
   }
?>




<form action="<?=h( Guestbook::$GUESTBOOK_URL )?>" method="POST">
   Name: <br>
   <input type="text" name="name" /><br>
   Comment: <br>
   <textarea id="comment-box" name="comment"></textarea> <br>
   <input type="submit" value="Submit" />
</form>

<p>
      <span id="status"></span>
    </p>
    <p>
      <button id="connect">WebUSB Connect</button>
    </p>


</div>
<?php
   our_footer();
?>
