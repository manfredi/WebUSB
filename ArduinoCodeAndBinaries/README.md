## Arduino code and binaires

Here is the code, and resulting binaries, that we compiled in the Arduino IDE to make our payloads.

The directories with "BluePill" in the name contain the code and binaries that run on [the website](https://outerspace.today/WebUSB/webdfuDriveby/).

For Windows and OS X, we adapted Samy Kamkar's [USBdriveby](http://samy.pl/usbdriveby/).

For Android, we created our own binary which opens GMail and sends a spam email.
