## Leonardo experiments
This directory, under arduino-gh-pages, is just demo code from [here](https://github.com/webusb/arduino).

It's not interesting for our project, and was just used to see how WebUSB worked at the very beginning of the semester.

To run it, we wired up a RGB light on a bread board. You can reconstruct that with the picture in Screenshots/RGB.jpg and the instructions [here](http://wiki.seeedstudio.com/wiki/Arduino_Sidekick_Basic_Kit#4_Rainbow_On_Desk:_Tricolor_LED).

The most interesting thing is in Screenshots, which we will put in our paper and presentation to show the popup with the URL suggestion that you get when you plug in a WebUSB device.
