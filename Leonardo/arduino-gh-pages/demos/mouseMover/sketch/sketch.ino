#include <WebUSB.h>
#include <Mouse.h>

const WebUSBURL URLS[] = {
  { 1, "outerspace.today" }, 
  { 0, "localhost:8000" },
};

const uint8_t ALLOWED_ORIGINS[] = { 1, 2 };

// arguments are: 
// list of URLs, number of URLS, landing page, 
// allowed origins, number of allowed origins
WebUSB WebUSBSerial(URLS, 2, 1, ALLOWED_ORIGINS, 2);

#define Serial WebUSBSerial

int pos[2];
int index;

void setup() {
  while (!Serial) {
    ;
  }
  Serial.begin(9600);
  Serial.write("Sketch begins.\r\n");
  Serial.flush();
  index = 0;
}

void loop() {
  if (Serial && Serial.available()) {
    pos[index++] = Serial.read();
    if (index == 2) {
      Mouse.move(pos[0], pos[1]);
      delay(1);
      Serial.print("Moved to ");
      Serial.print(pos[0]);
      Serial.print(", ");
      Serial.print(pos[1]);
      Serial.print(".\r\n");
      Serial.flush();
      index = 0;
    }
  }
}
