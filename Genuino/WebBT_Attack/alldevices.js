(function(){
	'use strict';
	class GetDevices {
		startconnect(){
			console.log('In here');
			navigator.bluetooth.requestDevice({
				acceptAllDevices: true,
				optionalServices: ['battery_service',0x181C] //Add all possible
			})
			.then(device => {
				console.log('Connecting to GATT Server');
				return device.gatt.connect();
			})
			.then(server => {
				console.log('Getting services');	
				return server.getPrimaryServices();
			})
			.then(services => {
				console.log('Getting Characteristics');
				let queue = Promise.resolve();
		  	services.forEach(service => {
      		queue = queue.then(_ => service.getCharacteristics().then(characteristics => {
			  		console.log('> Service: ' + service.uuid);
			    		characteristics.forEach(characteristic => {
          			console.log('>> Characteristic: ' + characteristic.uuid + ' ' + getSupportedProperties(characteristic));
        			});
      			}));
    		});
    		return queue;
			})
			.catch(error => {console.log('There is a problem' + error);})
		}

		getSupportedProperties(characteristic) {
  		let supportedProperties = [];
  		for (const p in characteristic.properties) {
    		if (characteristic.properties[p] === true) {
      		supportedProperties.push(p.toUpperCase());
    		}
  		}
  		return '[' + supportedProperties.join(', ') + ']';
		}

	}
	window.getdevices = new GetDevices();
})();
