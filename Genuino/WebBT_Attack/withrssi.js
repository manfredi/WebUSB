(function() {
  'use strict';

  class RelayClick {

    constructor() {
        this.deviceName = 'relays';
        this.serviceUUID = '917649a0-d98e-11e5-9eec-0002a5d5c51b';
        this.device = null;
        this.server = null;
        // The cache allows us to hold on to characeristics for access in response to user commands 
        this._characteristics = new Map();
    }

    interpretIBeacon(event) {
      var rssi = event.rssi;      //power at which the advertisement is received, measured in dBm.
      var appleData = event.manufacturerData.get(0x004C);
      if (appleData.byteLength != 23 ||
        appleData.getUint16(0, false) !== 0x0215) {
        console.log({isBeacon: false});
      }
      var uuidArray = new Uint8Array(appleData.buffer, 2, 16);
      var major = appleData.getUint16(18, false);
      var minor = appleData.getUint16(20, false);
      var txPowerAt1m = -appleData.getInt8(22);
      console.log({
          isBeacon: true,
          uuidArray,
          major,
          minor,
          pathLossVs1m: txPowerAt1m - rssi});
    }


    rssiconnect(){
      return navigator.bluetooth.requestDevice({
        filters: [{services: [0xffe5]}]
      })
      .then(device => {
        device.watchAdvertisements(); 
        /*
          there's no active chromium work on scanning now. Still desired,
          and there's still active Web Bluetooth work, but scanning will take a
          while. https://www.chromium.org/teams/device-team/device-okrs were our
          stated goals ~3 months ago. We're not hitting the Bluetooth targets due to
          other priorities. We'll be publishing OKRs again in July.
        */
        device.addEventListener('advertisementreceived', interpretIBeacon);
      })
      .catch(error => {console.log('There is a problem' + error);})
    }
}

window.rssiinfo = new RelayClick();

})();
