# webdfu attack
This uses Devianlai's [webdfu dfu-util](https://github.com/devanlai/webdfu) to download attack code to a WebUSB DFU device with the [dapboot](https://github.com/devanlai/dapboot) bootloader.

## Demos
### dfu driveby
A demo that downloads a harmless version of Samy Kamkar's [USBDriveby](http://samy.pl/usbdriveby/) to a dapboot device in order to attack OS X and Windows hosts using Devianlai's [dfu-util](https://devanlai.github.io/webdfu/dfu-util/). Can be found here:

https://outerspace.today/WebUSB/webdfuDriveby/

## Host-side implementation
WebUSB is currently only supported by Chromium / Google Chrome.

For Chrome to communicate with a USB device, it must have permission to access the device and the operating system must be able to load a generic driver that libusb can talk to.

On Linux, that means that the current user must have permission to access the device.

On Windows, that means that an appropriate WinUSB/libusb driver must first be installed. This can be done manually with programs such as [Zadig](http://zadig.akeo.ie/) or automatically (sometimes...) with [WCID](https://github.com/pbatard/libwdi/wiki/WCID-Devices)

The javascript DFU driver is ported from the excellent open-source software, [dfu-util](http://dfu-util.sourceforge.net/).

## Device-side implementation
Adding WebUSB support requires responding to requests for custom WebUSB descriptors, but otherwise requires no changes to existing USB functionality.

An example WebUSB-enabled USB DFU bootloader for the STM32F103 can be found here:

https://github.com/devanlai/dapboot

## Local testing
To test changes locally, you can run (in the directory with index.html):

    python -m SimpleHTTPServer


When running locally, the [origin trial token](https://github.com/jpchase/OriginTrials/blob/gh-pages/explainer.md) that enables WebUSB for this domain does not apply, so you must ensure that WebUSB is enabled by visiting chrome://flags and ensuring that the #enable-webusb and #enable-experimental-web-platform-features flags are enabled.

For additional tips and information about WebUSB, see this article:

https://developers.google.com/web/updates/2016/03/access-usb-devices-on-the-web#tips
