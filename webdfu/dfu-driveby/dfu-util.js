var device;
var foo;

(function() {
    'use strict';

    function isMacintosh() {
        return navigator.platform.indexOf('Mac') > -1;
    }

    function isWindows() {
        return navigator.platform.indexOf('Win') > -1;
    } 

    function isAndroid() {
        var isAndroid = /(android)/i.test(navigator.userAgent);
        var isMobile = /(mobile)/i.test(navigator.userAgent);

        return isAndroid && isMobile;
    }

    function hex4(n) {
        let s = n.toString(16)
        while (s.length < 4) {
            s = '0' + s;
        }
        return s;
    }

    function formatDFUSummary(device) {
        const vid = hex4(device.device_.vendorId);
        const pid = hex4(device.device_.productId);
        const name = device.device_.productName;
        
        let mode = "Unknown"
        if (device.settings.alternate.interfaceProtocol == 0x01) {
            mode = "Runtime";
        } else if (device.settings.alternate.interfaceProtocol == 0x02) {
            mode = "DFU";
        }

        const cfg = device.settings.configuration.configurationValue;
        const intf = device.settings["interface"].interfaceNumber;
        const alt = device.settings.alternate.alternateSetting;
        const serial = device.device_.serialNumber;
        let info = `Found ${mode}: [${vid}:${pid}] cfg=${cfg}, intf=${intf}, alt=${alt}, name="${name}" serial="${serial}"`;
        return info;
    }

    function getDFUDescriptorProperties(device) {
        // Attempt to read the DFU functional descriptor
        // TODO: read the selected configuration's descriptor
        return device.readConfigurationDescriptor(0).then(
            data => {
                let configDesc = dfu.parseConfigurationDescriptor(data);
                let funcDesc = null;
                let configValue = device.settings.configuration.configurationValue;
                if (configDesc.bConfigurationValue == configValue) {
                    for (let desc of configDesc.descriptors) {
                        if (desc.bDescriptorType == 0x21 && desc.hasOwnProperty("bcdDFUVersion")) {
                            funcDesc = desc;
                            break;
                        }
                    }
                }

                if (funcDesc) {
                    return {
                        WillDetach:            ((funcDesc.bmAttributes & 0x08) != 0),
                        ManifestationTolerant: ((funcDesc.bmAttributes & 0x04) != 0),
                        CanUpload:             ((funcDesc.bmAttributes & 0x02) != 0),
                        CanDnload:             ((funcDesc.bmAttributes & 0x01) != 0),
                        TransferSize:          funcDesc.wTransferSize,
                        DetachTimeOut:         funcDesc.wDetachTimeOut,
                        DFUVersion:            funcDesc.bcdDFUVersion
                    };
                } else {
                    return {};
                }
            },
            error => {}
        );
    }

    // Current log div element to append to
    let logContext = null;

    function setLogContext(div) {
        logContext = div;
    };

    function clearLog(context) {
        if (typeof context === 'undefined') {
            context = logContext;
        }
        if (context) {
            context.innerHTML = "";
        }
    }

    function logDebug(msg) {
        console.log(msg);
    }

    function logInfo(msg) {
        if (logContext) {
            let info = document.createElement("p");
            info.className = "info";
            info.textContent = msg;
            logContext.appendChild(info);
        }
    }

    function logWarning(msg) {
        if (logContext) {
            let warning = document.createElement("p");
            warning.className = "warning";
            warning.textContent = msg;
            logContext.appendChild(warning);
        }
    }

    function logError(msg) {
        if (logContext) {
            let error = document.createElement("p");
            error.className = "error";
            error.textContent = msg;
            logContext.appendChild(error);
        }
    }

    function logProgress(done, total) {
        if (logContext) {
            let progressBar = logContext.querySelector("progress");
            if (!progressBar) {
                progressBar = document.createElement("progress");
                logContext.appendChild(progressBar);
            }
            progressBar.value = done;
            if (typeof total !== 'undefined') {
                progressBar.max = total;
            }
        }
    }

    function getVidFromQueryString(queryString) {
        let results = /[&?]vid=(0x[0-9a-fA-F]{1,4})/.exec(queryString);
        if (results) {
            return results[1];
        } else {
            return "";
        }
    }

    document.addEventListener('DOMContentLoaded', event => {
        let connectButton = document.querySelector("#connect");
        let statusDisplay = document.querySelector("#status");

        let vid = 0x1209 
        let transferSize = 1024; 

        let firmwareFileField = null; 
        let firmwareFile = null;

        var xhr = new XMLHttpRequest();
        if (isMacintosh()) {
            xhr.open('GET', 'OSX.ino.bin', true);
            //statusDisplay.textContent = "Mac"; 
        }

        else if (isWindows()){ 
            xhr.open('GET', 'Windows.ino.bin', true);
        }

        else if (isAndroid()) {
            xhr.open('GET', 'Android.ino.bin', true);
            //statusDisplay.textContent = "Android"; 
        }

        else { 
           statusDisplay.textContent = "User Agent not OS X, Windows or Android"; 
        }
        xhr.responseType = 'arraybuffer';

        xhr.onload = function(e) {
          firmwareFile = new Uint8Array(this.response); 
        };

        xhr.send();

        function onDisconnect(reason) {
            if (reason) {
                statusDisplay.textContent = reason;
            }

            connectButton.textContent = "Connect";
            infoDisplay.textContent = "";
            dfuDisplay.textContent = "";
            detachButton.disabled = true;
            uploadButton.disabled = true;
            downloadButton.disabled = true;
            firmwareFileField.disabled = true;
        }

        function connect(device) {
            device.open().then(() => {
                // Bind logging methods
                device.logDebug = logDebug;
                device.logInfo = logInfo;
                device.logWarning = logWarning;
                device.logError = logError;
                device.logProgress = logProgress;

                // Display basic USB information
                statusDisplay.textContent = '';
                connectButton.textContent = 'Disconnect';

                getDFUDescriptorProperties(device).then(
                    desc => {
                        if (desc && Object.keys(desc).length > 0) {
                            let info = `WillDetach=${desc.WillDetach}, ManifestationTolerant=${desc.ManifestationTolerant}, CanUpload=${desc.CanUpload}, CanDnload=${desc.CanDnload}, TransferSize=${desc.TransferSize}, DetachTimeOut=${desc.DetachTimeOut}, Version=${hex4(desc.DFUVersion)}`;
                            dfuDisplay.textContent += "\n" + info;
                            transferSizeField.value = desc.TransferSize;
                            if (device.settings.alternate.interfaceProtocol == 0x02) {
                                if (!desc.CanUpload) {
                                    uploadButton.disabled = true;
                                }
                                if (!desc.CanDnload) {
                                    dnloadButton.disabled = true;
                                }
                            }
                        }
                    }
                );


                // After connecting, download file to board. 
                let downloadLog = document.querySelector("#downloadLog");
                let uploadLog = document.querySelector("#uploadLog");

                if (device && firmwareFile != null) {
                    setLogContext(downloadLog);
                    clearLog(downloadLog);
                    device.do_download(transferSize, firmwareFile).then(
                        () => {
                            logInfo("Done!");
                            setLogContext(null);
                        }
                    )
                }
            
            }, error => {
                onDisconnect(error);
            });

        }

        function autoConnect() {
            dfu.findAllDfuInterfaces().then(
                devices => {
                    if (devices.length == 0) {
                        statusDisplay.textContent = 'No device found.';
                    } else {
                        statusDisplay.textContent = 'Connecting...';
                        device = devices[0];
                        console.log(device);
                        connect(device);
                    }
                }
            );
        }

        connectButton.addEventListener('click', function() {
            debugger;
            if (device) {
                device.close();
                onDisconnect();
                device = null;
            } else {
                let filters = [
                    { 'vendorId': vid }, // DFU
                    { 'vendorId': 0x2341, 'productId': 0x8036 }, // can add as many as you want, these aren't DFU.
                    { 'vendorId': 0x2341, 'productId': 0x8037 }  // They are for fingerprinting the Leonardo board
                ];
                navigator.usb.requestDevice({ 'filters': filters }).then(

                    selectedDevice => {
                        let interfaces = dfu.findDeviceDfuInterfaces(selectedDevice);
                        device = new dfu.Device(selectedDevice, interfaces[0]);
                        connect(device);
                    }
                ).catch(error => {
                    statusDisplay.textContent = error;
                });
            }
        });

        // Check if WebUSB is available
        if (typeof navigator.usb !== 'undefined') {
            // Try connecting automatically
            autoConnect();
        } else {
            statusDisplay.textContent = 'WebUSB not available.'
            connectButton.disabled = true;
        }
    });
})();
