## 03.04.2017

The **LEDMagic Blue Light bulb** is bluetooth enabled. It can be paired with the official App for this bulb([LEDMagic blue for Android](https://play.google.com/store/apps/details?id=com.Zengge.BluetoothLigthDark&hl=en)) and we can control the color of the bulb through the app. This path of the switched-on light bulb and connecting to its official app can be viewed in [LaunchingInApp.jpg](https://gitlab.eurecom.fr/manfredi/WebUSB/blob/master/LEDMagicBulb/Images/LaunchingInApp.jpg) in clockwise direction. <img src="https://gitlab.eurecom.fr/manfredi/WebUSB/raw/master/LEDMagicBulb/Images/LaunchingInApp.jpg" height="600" width="600"/>



It is able to change the color of the bulb according to the Color wheel in the app as shown in [ChangingColors.jpg](https://gitlab.eurecom.fr/manfredi/WebUSB/blob/master/LEDMagicBulb/Images/ChangingColors.jpg)  <img src="https://gitlab.eurecom.fr/manfredi/WebUSB/raw/master/LEDMagicBulb/Images/ChangingColors.jpg" height="600" width="600"/>



However, changing the color without the official App was also possible by doing the following steps :



1. "Enable Bluetooth HCI Snoop log" in the Developer Options of the Mobile (I used Android 6.0.1) and record the bluetooth traffic when the 'Magic Blue' app is launched and change the color many times.
2. The recording will be available in /sdcard/btsnoop\_hci.log file in the mobile. Copy this file into the computer (I'm using Linux Operating System) and rename it to btsnoop\_hci.pcap. Then launch this file in Wireshark to analyse the traffic.
3. After filtering for 'Write Characteristic value commands' as shown in [btsnoophci\_ForWriteCommands.png](https://gitlab.eurecom.fr/manfredi/WebUSB/blob/master/LEDMagicBulb/Images/btsnoophci_ForWriteCommands.png) a pattern could be observed (for example, 56 03 19 0c 00 f0 aa in this image). For every packet there was a format *56 RR GG BB 00 f0 aa*. <img src="https://gitlab.eurecom.fr/manfredi/WebUSB/raw/master/LEDMagicBulb/Images/btsnoophci_ForWriteCommands.png" height="600" width="700"/>
4. Using this information, I connected to nRF Connect app and modified the bulb color by changing the RRGGBB values which is observed in [ModifiedColorWithRGBValues.jpg](https://gitlab.eurecom.fr/manfredi/WebUSB/blob/master/LEDMagicBulb/Images/ModifiedColorWithRGBValues.jpg). <img src="https://gitlab.eurecom.fr/manfredi/WebUSB/raw/master/LEDMagicBulb/Images/ModifiedColorWithRGBValues.jpg" height="600" width="600"/>



Now, the official app has features like Select Music (the bulb flashes with the rhythm of the song selected), Microphone Sensitivity, Camera (where the bulb will flash the color it sees in the camera). However, I still have to experiment accessing (and manipulating) these features without the official app.


Sources :

https://medium.com/@urish/reverse-engineering-a-bluetooth-lightbulb-56580fcb7546

